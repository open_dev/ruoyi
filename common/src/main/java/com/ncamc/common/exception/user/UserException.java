package com.ncamc.common.exception.user;

import com.ncamc.common.exception.base.BaseException;

/**
 * 用户信息异常类
 *
 * @author hugaoqiang
 */
public class UserException extends BaseException
{
    private static final long serialVersionUID = 1L;

    public UserException(String code, Object[] args)
    {
        super("user", code, args, null);
    }
}
