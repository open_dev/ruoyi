package com.ncamc.common.enums;

/**
 * 操作状态
 *
 * @author hugaoqiang
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
